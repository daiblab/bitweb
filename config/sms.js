module.exports = {
    sms: {
        ko: "님이 거래를 요청하였습니다.\n:",
        cn: "VTR交易已经开始。No.:\n"
    },
    notification: {
        ko: "님이 바로구매를 요청하셨습니다. 구매자님의 금액이 에스크로 되었습니다.\n",
        cn: "VTR我想继续进行交易。\n"
    }
};