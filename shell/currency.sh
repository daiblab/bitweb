#!/bin/bash
currency_list=("BTC" "ETH")

ipAddr=$(ip addr | grep "inet " | grep brd | awk '{print $2}' | awk -F/ '{print $1}')

if [ "$ipAddr" = "172.20.101.21" ]
then
    url="http://localhost:3000"
else
    url="http://localhost:3000"
fi

#임시로는 이렇게 처리
curl -X POST http://localhost/v1/charts/currency/BTC/KR
curl -X POST http://localhost/v1/charts/currency/ETH/KR
curl -X POST http://localhost/v1/charts/currency/XRP/KR
curl -X POST http://localhost/v1/charts/currency/LTC/KR
curl -X POST http://localhost/v1/charts/currency/EOS/KR
curl -X POST http://localhost/v1/charts/currency/BTC/CN
curl -X POST http://localhost/v1/charts/currency/ETH/CN
curl -X POST http://localhost/v1/charts/currency/XRP/CN
curl -X POST http://localhost/v1/charts/currency/LTC/CN
curl -X POST http://localhost/v1/charts/currency/EOS/CN
#curl -X POST http://localhost:3000/v1/charts/currency/BTC/JP
#curl -X POST http://localhost:3000/v1/charts/currency/ETH/JP
#curl -X POST http://localhost:3000/v1/charts/currency/BTC/EN
#curl -X POST http://localhost:3000/v1/charts/currency/ETH/EN